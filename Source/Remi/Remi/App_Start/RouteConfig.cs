﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Remi
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "GCPublicEvents",
                url: "gcalendar/events/{page}",
                defaults: new { controller = "GCalendar", action = "GetPublicEvents",
                    page = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "User-Auth",
                url: "auth/user/",
                defaults: new { controller = "Authentication", action = "IsUserAuthenticated" }
            );

            routes.MapRoute(
                name:"User-Create",
                url: "create/user/",
                defaults: new {controller = "Authentication", action = "ValidateNewUser"}
            );

            routes.MapRoute(
                name: "GetUserProfile",
                url: "user/prof/{id}",
                defaults: new { controller = "User", action = "UserPreferences"}
            );

            routes.MapRoute(
                name: "EditUserProfile",
                url: "user/edit/{id}",
                defaults: new { controller = "User", action = "sumbitUserProfileChanges" }
            );

            routes.MapRoute(
                name: "LanguageResponse-Text",
                url: "language/text/",
                defaults: new { controller = "Language", action = "ProcessText" }
            );

            routes.MapRoute(
                name: "Language-Jokes",
                url: "language/joke/",
                defaults: new { controller = "Language", action = "TellJoke" }
            );

            routes.MapRoute(
                name: "ShowWeather",
                url: "weather/{page}",
                defaults: new { controller = "Weather", action = "ShowWeather", page = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "GetDefinition",
                url: "dictionary/define/",
                defaults: new { controller = "Dictionary", action = "GetDefinition" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
