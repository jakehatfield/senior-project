Feature: As a user, I want to ask Remi to find information on a 
    subject for me so I don't have to search for it in a browser.
 
    Scenario: User requests a search for Keanu Reeves it returns 
        useful information about that entity.
        Given the Bing Enity Search Key is valid
            And the user user is logged in.
        When "Who is Keanu Reeves?" is typed by the user
            And some other action
        And they press send.
        Then LUIS returns the intent "Search"
        And Bing Entity Search returns name as "Keanu Reeves".
        
    Scenario: User requests a search for Halsey it returns 
        useful information about that entity.
        Given the Bing Enity Search Key is valid
            And the user user is logged in.
        When "Who is Halsey?" is typed by the user
            And some other action
        And they press send.
        Then LUIS returns the intent "Search"
        And Bing Entity Search returns name as "Halsey".
        
    Scenario: User requests a search for Louvre it returns 
        useful information about that entity.
        Given the Bing Enity Search Key is valid
            And the user user is logged in.
        When "What is the Louvre?" is typed by the user
            And some other action
        And they press send.
        Then LUIS returns the intent "Search"
        And Bing Entity Search returns name as "Louvre".