﻿// QUnit Tests for sign out
QUnit.module('User Signout');

QUnit.test('Parse intent from a correct dumby signout JSON Object', function (assert) {
    // Arrange
    var json = {
        "query": "Log out",
        "topScoringIntent": { "intent": "User.Logout", "score": 0.9614282 },
        "intents": [
            {
                "intent": "User.Logout",
                "score": 0.9614282
            }, {
                "intent": "None",
                "score": 0.0446737222
            },
            {
                "intent": "RemiGreeting",
                "score": 0.0226409659
            },
            {
                "intent": "GetWeather",
                "score": 0.005408189
            },
            {
                "intent": "GetDefinition",
                "score": 0.004625437
            },
            {
                "intent": "RemiJoke",
                "score": 0.00249553355
            },
            {
                "intent": "GetCalendarData",
                "score": 0.001955232
            }
        ],
        "entities": []
    };

    var expected = "User.Logout"

    // Act
    var actual = getIntent(json);

    // Assert
    assert.equal(actual, expected, "Got the expected intent, 'User.Logout', from the JSON Object");
    assert.notEqual(actual, "RemiGreeting", "Did not get 'RemiGreeting' intent from the JSON Object");
    assert.notEqual(actual, "None", "Did not get 'None' intent from the JSON Object");
    assert.notEqual(actual, "GetWeather", "Did not get 'GetWeather' intent from the JSON Object");
    assert.notEqual(actual, "GetDefinition", "Did not get 'GetDefinition' intent from the JSON Object");
    assert.notEqual(actual, "RemiJoke", "Did not get 'RemiJoke' intent from the JSON Object");
    assert.notEqual(actual, "GetCalendarData", "Did not get 'GetCalendarData' intent from the JSON Object");
    assert.ok
});

QUnit.test('Parse entities from Dummy Search JSON Object', function (assert) {

    // Arrange
    var json = {
        "query": "Who is Eminem?",
        "topScoringIntent":
        {
            "intent": "Search",
            "score": 0.895334244
        },
        "intents": [
            {
                "intent": "Search",
                "score": 0.895334244
            },
            {
                "intent": "GetDefinition",
                "score": 0.0221064817
            },
            {
                "intent": "None",
                "score": 0.01061958
            },
            {
                "intent": "GetWeather",
                "score": 0.008669283
            },
            {
                "intent": "User.Logout",
                "score": 0.00307040033
            },
            {
                "intent": "RemiJoke",
                "score": 0.002567114
            },
            {
                "intent": "RemiGreeting",
                "score": 0.00172281731
            },
            {
                "intent": "GetCalendarData",
                "score": 0.00162721379
            }
        ],
        "entities": [
            {
                "entity": "eminem",
                "type": "Subject",
                "startIndex": 7,
                "endIndex": 12,
                "score": 0.945739269
            }
        ]
    };

    var expected = [
        {
            "entity": "eminem",
            "type": "Subject",
            "startIndex": 7,
            "endIndex": 12,
            "score": 0.945739269
        }
    ];

    var expectedName = "eminem";
    var expectedType = "Subject";

    // Act
    var actual = getEntities(json);
    var actualName = actual[0].entity;
    var actualType = actual[0].type;

    // Assert
    assert.propEqual(actual, expected, "Got the entities list from the JSON Object.");
    assert.equal(actualName, expectedName, "Got the correct Entity Name from Entities.");
    assert.equal(actualType, expectedType, "Got the correct Entity Type from Entities.");
});

QUnit.test('Ensure that the modal pops up when the function for User.Logout intent is called', function (assert) {
    // Arrange - Takes no input, just requires arrange expected output.
    var expected = "block";

    // Act
    showLogout();
    var actual = document.getElementById('logoutConfirmation').style.display;

    // Assert
    assert.equal(actual, expected, "The modal is shown as a block display type");
    assert.notEqual(actual, "none", "The modal is not hidden");
    assert.notEqual(actual, "flex", "The modal is not shown as a flex display type");
});