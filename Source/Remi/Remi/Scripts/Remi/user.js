﻿//Global Variables Here
var currUser;
var home;
var calendarID;
var voicePref;
var uid;

//Listeners Here

//clicked the login/user link
$('#userLogin').click(userLoginPrompt);
//clicked the logout link
$('#userLogout').click(showLogout);
// Logout confirmation button
$('#logoutConfirm').click(userLogout);
$('#logoutCancel').click($('#logoutConfirmation').modal('hide'));
// Login submit button
$('#loginSubmit').click(userLogin);
// Create account link
$('#createLink').click(createAccount);
// Create account submit button
$('#createSubmit').click(validateAccount);
// Edit user profile button
$('#editProfileButton').click(openEditProfile);
// Edit profile cancel button
$('#editProfileCancelButton').click(cancelEdit);
// Submit user profile changes button
$('#submitChangesButton').click(submitChanges);

// Functions Here

// Function to prompt the user for login or show them their profile if already logged int
function userLoginPrompt() {
    var status = $('#userLogin').val();
    console.log(status);
    if (status == 0) {
        console.log('Not currently logged in.');
        showLoginPrompt();
    }
    else {
        // Show the profile info
        //populate the modal.
        generateProfileView();
    }
}

function generateProfileView() {
    var profile = '<strong>Name:</strong> ' + currUser + '</br>'
        + '<strong>Calendar ID:</strong> ' + calendarID + '</br>'
        + '<strong>Home Location:</strong> ' + home + '</br>'
        + '<strong>Voice Preference:</strong> ' + voicePref + '</br>';
    $('#userProfileData').empty();
    $('#userProfileData').append(profile);
    $('#userProfile').modal('show')
    console.log(currUser + ' is currently logged in.');
    generateForm();
}

// Function to show the login modal
function showLoginPrompt() {
    $('#loginPrompt').modal('show');
}

//After we have authentication, refactor to change this once the user it logged in
//For testing we will just fill the user info nav item with a test user.
function userLogin() {
    console.log('Attempt to log in a user...');
    // Get the values from the input fields
    var eField = $('#emailInputLogin').val();
    var pField = $('#passwordInputLogin').val();
    // Send the input to the Auth controller to be checked
    var source = "/auth/user/?email=" + eField + "&attemptedPassword=" + pField;
    $.ajax({
        type: "GET",
        dataType: "json",
        url: source,
        success: userLoginHelper,
        error: logFailure
    });

}
function userLoginHelper(data) {
    //Check if login was valid or not
    console.log(data);
    authCheck = data.Authorized;
    if (authCheck) {
        console.log("Authorized User!");
        // Get the authenticated user's id
        uid = data.UID;
        console.log("UID = " + uid);
        // Make the user login value true
        $('#userLogin').val(uid); //indicate in the value that the user is logged in with UserID, if 0, not logged in
        // Send the authenticated user's id to the user controller to get profile information
        source = "/user/prof/" + String(uid);
      console.log("source: " + source);
        $.ajax({
            type: "GET",
            dataType: "json",
            url: source,
            success: getDetails,
            error: logFailure
        });
    }
    else {
        console.log("Authorization Failed");
        // Get the Error message
        msg = data.ErrorMessage;
        console.log("Error: " + msg);

        //Display Error Message - Alert for now
        alert(msg);
    }
}
// Function to get the user's profile details
function getDetails(data) {
    console.log(data);
    // Get the user's profile data (saved to local mem as variables)
    currUser = data.Alias;
    home = data.HomeLocation;
    calendarID = data.CalendarID;
    voicePref = data.VoicePref;

    // Change the "Login" link to display the current user's alias
    $('#userLogin').html('<a class="navbar-brand" href="#">' + currUser + '</a > ');
    // Display the Logout link
    $('#userLogout').css('display', 'flex');

    //Change the href for #userLogin to trigger the show user profile modal or make a conditional function for the listener.

    // Hide login modal
    $('#loginPrompt').modal('hide');
}

//After we hat authentication, refactor to fall in-line with needs
//Function to logout the test user that we "logged in"
function userLogout() {
    console.log('Log out the user...')
    // Stop displaying the logout link
    $('#userLogout').css('display', 'none'); 
     // Indicate in the value that user is logged out
    $('#userLogin').val(false);
     // Change the user info back to login
    $('#userLogin').html('<a class="navbar-brand" href="#">Login</a>');
    $('#logoutConfirmation').modal('hide');
    clearRemiOutput();
    currUser = "";
    home = "";
    calendarID = "";
    voicePref = "";
    uid = null;
}

// Function to show the logout modal
function showLogout() {
    $('#logoutConfirmation').modal('show');
}

// All the create account logic goes here ---------------

// Switch out modals
function createAccount() {
    $('#loginPrompt').modal('hide');
    $('#createPrompt').modal('show');
}

// Validate that the form is filled in 
function validateAccount() {
    console.log("Validating...");
    if (!checkFormFilled()) {
        registerFail();
        return;
    }
    // Send it over to the actual logic
    else sendCreate();
}

// A function to check form fields
function checkFormFilled() {
    if ($("#usernameInput").val() == "" || $("#emailInput").val() == "" || $("#passwordInput").val() == ""
        || $("#passwordConfirmInput").val() == "" || $("#aliasInput").val() == "" || $("#homeInput").val() == ""
        || $("#calendarInput").val() == "")
        return false;
    return true;
}

// Parse our variables and send the AJAX request
function sendCreate() {
    console.log("Attempting to create an account...");
    var username = $('#usernameInput').val();
    var email = $('#emailInput').val();
    var p1 = $('#passwordInput').val();
    var p2 = $('#passwordConfirmInput').val();
    var alias = $('#aliasInput').val();
    var home = $('#homeInput').val();
    var cal = $('#calendarInput').val();

    // generate us a very silly looking query string
    var source = '/create/user/?username=' + username + '&email=' + email + '&password=' + p1 + '&confPassword=' + p2 + '&alias=' + alias + '&homeLoc=' + home + '&calID=' + cal; 
    $.ajax({
        type: "GET",
        dataType: "json",
        url: source,
        success: confirmRegistration,
        error: registerFail
    });
}

// Confirmation to the user and reset the fields
function confirmRegistration(newUser) {
    if (newUser.UserID != -1) {
        $('#createAccountForm').empty();
        $('#createPrompt').modal('hide');
        alert("Registration successful!");
    }
    else registerFail();
}

// Something went wrong
function registerFail() {
    alert("Something went wrong! Please make sure all fields are filled out correctly and try again");
}

function openEditProfile() {
    $('#userProfile').modal('hide');
    $('#editUserProfile').modal('show');
}

function cancelEdit() {
    $('#editUserProfile').modal('hide');
    $('#userProfile').modal('show');
}

// Submit edit user profile changes
function submitChanges() {
    //Variables to set new values
    var newAlias = $('#editNameInput').val();
    var newCalID = $('#editCalendarInput').val();
    var newHomeLoc = $('#editHomeLocationInput').val();
    var newVoicePref = $('#voicePref').val();

    // If not empty (changes made) change the value of the var
    if (newAlias != "") {
        console.log("New Alias: " + newAlias);
        alias = newAlias;
        currUser = newAlias;
        $('#userLogin').html('<a class="navbar-brand" href="#">' + alias + '</a > ');
    }
    else { newAlias = currUser; } // If no changes set New var to current value (Optimize later?)

    // If not empty (changes made) change the value of the var
    if (newCalID != "") {
        console.log("New CalanderID: " + newCalID);
        calendarID = newCalID;
    }
    else { newCalID = calendarID; } // If no changes set New var to current value (Optimize later?)

    // If not empty (changes made) change the value of the var
    if (newHomeLoc != "") {
        console.log("New Home Location: " + newHomeLoc);
        home = newHomeLoc;
    }
    else { newHomeLoc = home; } // If no changes set New var to current value (Optimize later?)
    
    // If the voice pref is changed set it in voicePref global var
    if (newVoicePref != "No Change") {
        console.log("New Voice Preference: " + newVoicePref);
        // Make imediate changes to profile vaules stored in global vars
        // Required to make imediate change in voice selection
        voicePref = newVoicePref;
    }
    else { newVoicePref = voicePref; } // If no changes set New var to current value (Optimize later?)
    

    //Call the User controller to make appropriate changes to the profile DB entry
    var changes = '?alias=' + newAlias + '&homeLoc=' + newHomeLoc + '&calendarID=' + newCalID + '&voicePref=' + newVoicePref;
    var source = '/user/edit/' + uid + changes

    // we are not expecting data back so datatype and type are not required.
    $.ajax({
        url: source,
        success: submitChangesHelper,
        error: logFailure
    });
}

function submitChangesHelper() {
    $('#editUserProfile').modal('hide');
    userLoginPrompt();
}

// Generate the form for users to edit their profile
function generateForm() {
    var form = '<form>'
        + '<div class="form-group">'
        + '<label for="nameEdit"><b>Name</b></label>'
        + '<input type="text" placeholder="' + currUser + '" name="nameEdit" class="form-control" id="editNameInput" />'
        + '</div>'
        + '<div class="form-group">'
        + '<label for="calendarEdit"><b>Calendar ID</b></label>'
        + '<input type="text" placeholder="' + calendarID + '" name="calendarEdit" class="form-control" id="editCalendarInput" />'
        + '</div>'
        + '<div class="form-group">'
        + '<label for="homeLocationEdit"><b>Home Location</b></label>'
        + '<input type="text" placeholder="' + home + '" name="homeLocationEdit" class="form-control" id="editHomeLocationInput" />'
        + '</div>'
        + '<div class="form-group">'
        + '<label for="voicePref"> Prefered Voice:</label>'
        + '<select class="form-control" id="voicePref">'
        + '<option>No Change</option>'
        + '<option>UK English Female</option>'
        + '<option>UK English Male</option>'
        + '<option>US English Female</option>'
        + '<option>Spanish Female</option>'
        + '<option>French Female</option>'
        + '<option>Deutsch Female</option>'
        + '<option>Italian Female</option>'
        + '<option>Greek Female</option>'
        + '<option>Hungarian Female</option>'
        + '<option>Turkish Female</option>'
        + '<option>Russian Female</option>'
        + '<option>Dutch Female</option>'
        + '<option>Swedish Female</option>'
        + '<option>Norwegian Female</option>'
        + '<option>Japanese Female</option>'
        + '<option>Korean Female</option>'
        + '<option>Chinese Female</option>'
        + '<option>Hindi Female</option>'
        + '<option>Serbian Male</option>'
        + '<option>Croatian Male</option>'
        + '<option>Bosnian Male</option>'
        + '<option>Romanian Male</option>'
        + '<option>Catalan Male</option>'
        + '<option>Australian Female</option>'
        + '<option>Finnish Female</option>'
        + '<option>Afrikans Male</option>'
        + '<option>Albanian Male</option>'
        + '<option>Arabic Male</option>'
        + '<option>Armenian Male</option>'
        + '<option>Czech Female</option>'
        + '<option>Danish Female</option>'
        + '<option>Esperanto Male</option>'
        + '<option>Hatian Creole Female</option>'
        + '<option>Icelandic Male</option>'
        + '<option>Indonesian Female</option>'
        + '<option>Latin Female</option>'
        + '<option>Latvian Male</option>'
        + '<option>Macedonian Male</option>'
        + '<option>Moldavian Male</option>'
        + '<option>Montenegrin Male</option>'
        + '<option>Polish Female</option>'
        + '<option>Portuguese Female</option>'
        + '<option>Slovak Female</option>'
        + '<option>Spanish Latin American Female</option>'
        + '<option>Swahili Male</option>'
        + '<option>Tamil Male</option>'
        + '<option>Thai Female</option>'
        + '<option>Vietnamese Male</option>'
        + '<option>Welsh Male</option>'
        + '</select>'
        + '</div>'
        + '</form>';
    $('#editData').empty();
    $('#editData').append(form);
}
