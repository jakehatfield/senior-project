﻿// --- Global Variables here ---
//...

// --- Listeners here ---
// Toggle button for Remi's text output
$('#checktextout').click(toggleRemisTextOutput);
// Toggle radio buttons for Remi's text input
$('#checktextin').click(toggleTextInput);
// Toggle Checkbox for the Info Pane
$('#infoPaneDisplay').click(toggleInfoPane);


//Function to toggle the visablility of Remi's text output
function toggleRemisTextOutput() {
    var toggle = $('input[name=toggletextoutput]:checked').length;
    if (toggle > 0) {
        $('#remiChatOutput').css('display', 'block');
        //console.log('text output toggle: on');
    }
    else {
        $('#remiChatOutput').css('display', 'none');
        //console.log('text output toggle: off');
    }
}

//Funcion to toggle Remi's text input
function toggleTextInput() {
    var toggle = $('input[name=toggletextinput]:checked').length;
    if (toggle > 0) {
        $('#remitextinputform').css('display', 'flex');
        //console.log('text input toggle: on');
    }
    else {
        $('#remitextinputform').css('display', 'none');
        //console.log('text input toggle: off');
    }
}

function toggleInfoPane() {
    var toggle = $('input[name=toggleInfoPane]:checked').length;
    if (toggle == 0) {
        $('#fillerPane').css('display', 'block');
        //console.log('text input toggle: on');
    }
    else {
        $('#fillerPane').css('display', 'none');
        //console.log('text input toggle: off');
    }
}

