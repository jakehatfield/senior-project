﻿//Global Variables
//. . .

//API stuff

//Listeners Here
//. . .

//Functions Here
//Function to get the word entity from the LUIS call and send it to the dictionary API
function getDefinition(entities) {
    
    try {
        var entitiesLength = entities.length;
        //if enitities length is 0 then there was no word found, print an error
        if (entitiesLength === 0) {
            console.log("getDefinition: no word found!");
        }
        //else, there was a word found, send it to dictionary API for lookup
        else {
            var word = entities[0][1];
            console.log("Word: " + word);
        }
    }
    catch (error) {
        console.log("Error: " + error);
    }

    var source = "dictionary/define/";
    
    $.ajax({
        type: "GET",
        dataType: "json",
        url: source,
        data: { "word" : word },
        success: showDefinition,
        error: logFailure
    });

}

function showDefinition(response) {
    var outputString = "";
    console.log(response);

    if (response === null) {
        outputString = "Sorry, I don't know that word. Try again?";
        outputRemiChat(outputString);
    }
    else {
        // Gets the word requested, for use in the output
        // Capitalizes the first letter, to make it nice
        var word = response.results[0].word;
        word = word.charAt(0).toUpperCase() + word.slice(1);
        // Gets only the first returned definition, for simplicity's sake
        var definition = response.results[0].lexicalEntries[0].entries[0].senses[0].definitions[0];
        outputString = "'" + word + "'" + " is defined as: " + definition;
        outputRemiChat(outputString);
    }
}