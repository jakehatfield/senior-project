﻿// --- Global Variables here ---
//..

// --- Listeners here ---

// --- Variables ---
// variable to store the calendar type.
var calType;
//--- Functions here ---

// Function to send a request to our server to request Event data from Google
function getPublicEvents(entities) {
    // console.log(entities);
    // using custom route to get public events from a Google Calendar
    //will add a way to specify the calendar ID in later phase of dev.
    var source = "/gcalendar/events/";
    var first = true;
    for (var i = 0; i < entities.length; ++i) {
        if (entities[i].type === "Calendar.Position") {
            source += (first ? "?" : "&");
            var e = entities[i].entity.toLowerCase();
            switch (e) {
                case "next":
                    source += "position=1";
                    break;
                case "last":
                    source += "position=-1"
                    break;
                default:
                    source += "position=0"
                    break;
            }
            first = false;
        }
        else if (entities[i].type === "Calendar.TimeSpan") {
            source += (first ? "?" : "&");
            source += "span=" + entities[i].entity.toLowerCase();
            first = false;
        }
        else if (entities[i].type === "Calendar.Type") {
            calType = entities[i].entity.toLowerCase();
        }
    }
    console.log(source);

    // AJAX sends the events request to our server
    $.ajax({
        type: "GET",
        dataType: "json",
        url: source,
        success: displayPublicEvents,
        error: APIErrorEvents
    });

}

// Function to display the results for Google Calendar Events (Public)
function displayPublicEvents(events) {
    // Show the response from the server for testing and demonstration purposes.
    console.log("Calendar response: ");
    console.log(events);

    // Empty the filler pane to prepare for new info
    $('#canvasOverlay').empty();
    console.log("Calendar type: " + calType);
    if (calType === "agenda") {
        var agenda = "<ol>";
        for (let event of events) {
            agenda += '<li>'
            + '<em>' + event.EventName + '</em>'
            + ' on ' + event.EventDate
            + ' from ' + event.EventStartTime + " to " + event.EventEndTime
            + '</li>';
        }
        agenda += '</ol>';
        $('#canvasOverlay').append(agenda);
    }
    else {
        // Begin an output HTML string
        var table = "<table class='table table-bordered table-responsive'><thead>";
        var i = 0;

        // Run through the events
        // A lot of this logic looks ugly so that the calendar will wrap after four tiles
        while (i < events.length) {
            table = table.concat("<tr>");
            var j = 0;

            // We will be using x to access the actual events
            var x = i
            // Four event headers
            for (j; j < 4; j++) {
                // If the event doesn't exist, break
                if (events[x] == null) break;
                // Otherwise add it in a th
                table = table.concat("<th style='width:25%;'><h2>" + events[x].EventDate + "</h2></th>");
                // Advance x
                x++
            }
            table = table.concat("</thead><tbody>")

            // Reset x so we can access the same four events
            x = i;
            // Four events
            for (j; j < 8; j++) {
                // If the event doesn't exist, break
                if (events[x] == null) break;
                // Otherwise, add it in a td
                table = table.concat("<td><h4>" + events[x].EventName + "</h4>");
                table = table.concat("<br/>" + events[x].EventStartTime + "-" + events[x].EventEndTime + "</td>");
                // Advance x
                x++;
            }
            table = table.concat("</tbody></tr>");

            // Advance i by 4 so we get a new set of events
            i += 4;
        }
        $('#canvasOverlay').append(table);
    }
    // wait a second before triggering the overlay
    setTimeout(function () {
        $('#canvasData').modal('show');
    }, 0.5 * 1000);

    outputRemiChat('Sure thing! Here is the information you requested.');
}

//Function to tell the Dev that an Error happened while executing 
//the Google Calendar Events (Public) Request
//**Some refactoring may happen here if we can make a generic error handler for requests!
function APIErrorEvents() {
    console.log("The Google Calendar API Fizzled out! - Public Events");
}