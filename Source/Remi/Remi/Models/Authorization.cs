using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remi.Models
{
    // <summary>
    // Class for authentication of user at time of login
    // </summary>
    public class Authorization
    {
        public bool AuthorizedUser { get; set; }
        public string FailureMessage { get; set; }
    }
}