-- DB-Path: (localdb)\MSSQLLocalDB ; DB-Name: RemiUsers

CREATE TABLE dbo.Users
(
	UserID				INT IDENTITY(1,1) NOT NULL,
	UserName			VARCHAR(50) NOT NULL,
	Email				VARCHAR(50) UNIQUE NOT NULL,
	CipheredPassword	VARCHAR(130) NOT NULL,
	PasswordSalt		VARCHAR(64) NOT NULL,
	CONSTRAINT[PK_dbo.Users] PRIMARY KEY CLUSTERED (UserID ASC)
);

CREATE TABLE dbo.Profiles
(
	ProfileID	INT IDENTITY(1,1) NOT NULL,
	Alias		VARCHAR(50) NOT NULL,
	VoicePref	VARCHAR(50) NOT NULL,
	HomeLoc		VARCHAR(50) NOT NULL,
	HomeCity	VARCHAR(50) NOT NULL,
	HomeState	VARCHAR(50) NOT NULL,
	CalendarID	VARCHAR(1024) NOT NULL, -- This is set to 1024 chars because of the max length specified by Google documentation at: https://developers.google.com/calendar/v3/reference/events/insert (see id entry)
	UserID		INT UNIQUE NOT NULL,
	CONSTRAINT[PK_dbo.Profiles] PRIMARY KEY CLUSTERED (ProfileID ASC),
	CONSTRAINT[FK_dbo.Users] FOREIGN KEY (UserID)
		REFERENCES dbo.Users (UserID)
		ON DELETE CASCADE
);

INSERT INTO dbo.Users (UserName, Email, CipheredPassword, PasswordSalt) VALUES
('TestUser', 'test@test.com', 'Xyum7g+Tt7u+ZigACded/Hdl8UzY7l5CwROQ7968jfJjQWSiWSfIn/TNRmkKkiLM7Xo8G+1S20WuBOFPfBwWXanuuYekV8AlWvuKG+xI4xIu8Vwkh4UjDk/KPU9VVLTd', 'PasswordSalt'), /* password = Hello@2 */
('SomeoneElse', 'fake@email.com', '3LF5N4lwJZRl/IGXNqOqqPtraj1qgBR4fkbmVCLYLe+Z4hafvxaOD1iZWhk0D17Z3eQ/lvt8EdELElv81xNUi1PRNwzRDWpTdX/wGh5BMrm6/jXYZmkuPXgNQt8qZrva', 'PasswordSalt')  /* password = Hello@2 */
;

INSERT INTO dbo.Profiles (Alias, VoicePref, HomeLoc, HomeCity, HomeState, CalendarID, UserID) VALUES
('Test', 'UK English Female', 'Monmouth, Oregon', 'Monmouth', 'Oregon', 'reviaitest@gmail.com', 1),
('Human Entity', 'UK English Male', 'Atlantic City, New Jersey', 'Atlantic City', 'New Jersey', 'reviaitest@gmail.com', 2)
;