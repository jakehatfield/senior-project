namespace CS461_M3_Cross_Country.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Session
    {
        [Key]
        [Display(Name ="Training ID")]
        public int TrainingID { get; set; }

        [Column(TypeName = "date")]
        [Display(Name ="Date")]
        [DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime TrainingDate { get; set; }

        [Display(Name = "Time")]
        public TimeSpan TrainingTime { get; set; }

        [Required]
        [Display(Name = "Heart Rate")]
        public int HeartRate { get; set; }

        [Column(TypeName = "text")]
        [Required]
        [Display(Name = "GPS Data")]
        public string GPSData { get; set; }

        public double Distance { get; set; }

        [Display(Name = "Athlete ID")]
        public int AthleteID { get; set; }

        public virtual Athlete Athlete { get; set; }
    }
}
