﻿using CS461_M3_Cross_Country.DAL;
using CS461_M3_Cross_Country.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace CS461_M3_Cross_Country.Controllers
{
    public class AthletesController : Controller
    {
        private XCDatabaseContext db = new XCDatabaseContext();

        /// <summary>
        /// Provides a list of all the athletes in the system.
        /// </summary>
        /// <returns>Returns a view containing a list of athletes.</returns>
        public ActionResult Index()
        {
            var athletes = db.Athletes.ToList();
            return View(athletes);
        }

        /// <summary>
        /// Provides a summary of an athlete.
        /// </summary>
        /// <param name="id">The identification number of the athlete.</param>
        /// <returns>A view containing the specified athlete's information.</returns>
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Athlete athlete = db.Athletes.Find(id);
            if (athlete == null)
            {
                return HttpNotFound();
            }
            return View(athlete);
        }

        /// <summary>
        /// Provides the creation tool to add a new athlete for the database.
        /// </summary>
        /// <returns>Returns a view </returns>
        public ActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// Adds a new athlete to the database.
        /// </summary>
        /// <param name="athlete">The athlete being created.</param>
        /// <returns>REsirtectToAction to the index that contains the list of athletes.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AthleteID,FirstName,LastName,Age,Height,Weight")] Athlete athlete)
        {
            try
            {
                db.Athletes.Add(athlete);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Create");
            }
        }

        /// <summary>
        /// Allow the editing of an athlete in the database.
        /// </summary>
        /// <param name="id">The identification number of the athlete being edited.</param>
        /// <returns>A view containing the athlete being edited.</returns>
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Athlete athlete = db.Athletes.Find(id);
            if (athlete == null)
            {
                return HttpNotFound();
            }
            return View(athlete);
        }

        /// <summary>
        /// Edits the athlete in the system
        /// </summary>
        /// <param name="athlete">The athlete being edited.</param>
        /// <returns>RedirectToAction to the list of Athletes.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AthleteID,FirstName,LastName,Age,Height,Weight")] Athlete athlete)
        {
           if(ModelState.IsValid)
            { 
                db.Entry(athlete).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            
            return RedirectToAction("Edit", athlete.AthleteID);
        }

        /// <summary>
        /// Editor that enabled the removal of an athlete from the system.
        /// </summary>
        /// <param name="id">The identification number of the athlete.</param>
        /// <returns>A view containing the athlete that is going to be removed.</returns>
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Athlete athlete = db.Athletes.Find(id);
            if (athlete == null)
            {
                return HttpNotFound();
            }
            return View(athlete);
        }

        /// <summary>
        /// Deletes the specified athlete from the system.
        /// </summary>
        /// <param name="id">The identification number of the athlete to remove.</param>
        /// <returns>REdirectToAction send the user to the list of athletes when complete.</returns>
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            if(ModelState.IsValid)
            { 
                Athlete athlete = db.Athletes.Find(id);
                db.Athletes.Remove(athlete);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            
            return RedirectToAction("Delete", id);
        }

        public ActionResult Login()
        {
            var athletes = db.Athletes.ToList();
            return View(athletes);
        }

        public ActionResult AthleteDetails(int id)
        {
            
            Athlete athlete = db.Athletes.Find(id);
            ViewBag.AthleteName = athlete.FirstName + " " + athlete.LastName;
            ViewBag.AthleteID = athlete.AthleteID;

            var sessions = db.Sessions.Where(s => s.AthleteID == athlete.AthleteID).ToList();
            return View(sessions);
        }

        /// <summary>
        /// Get the session speed information for the specified athlete.
        /// </summary>
        /// <param name="id">The database identification number of the athlete.</param>
        /// <returns></returns>
        public JsonResult AthleteSpeeds(int id)
        {
            var sessions = db.Sessions.Where(s => s.Athlete.AthleteID == id).ToList();
            List<Speed> speeds = new List<Speed>();
            // For each item in the query create a new speed item.
            foreach (var item in sessions)
            {
                Speed speed = new Speed();
                speed.Date = item.TrainingDate.ToShortDateString();
                speed.Distance = item.Distance;
                speed.Time = item.TrainingTime;
                // Add the speed item to the list of speeds for that athlete.
                speeds.Add(speed);
            }
            
            return Json(speeds, JsonRequestBehavior.AllowGet);
        }
    }
}
